export const decompress = function(hierarchie, dataArr) {
	let o = new Array(dataArr.length)
	dataArr.forEach(function(strOrArrVal, j) {
		let p = o[j] = {}
		hierarchie.forEach(function(nameOrObjHier, posProp) {
			if (nameOrObjHier.split) {
				p[nameOrObjHier] = strOrArrVal[posProp]
			} else {
				let key = Object.keys(nameOrObjHier)[0]
				p[key] = decompress(nameOrObjHier[key], strOrArrVal[posProp])
			}
		})
	})
	return o
}

export default function getdDataRequest(req, callback) {
	const url = 'http://localhost/apero_web/static/api.php?json=' + JSON.stringify(req)

	const xhttp = new XMLHttpRequest()
	xhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) {
			const json = JSON.parse(xhttp.responseText)

			if (req.fetch) {
				if (!json.response || !json.response.data)
					return
				let decode = decompress(json.response.decode, json.response.data)
				callback(decode)
			} else {
				callback(json)
			}
		}
	}
	xhttp.open("GET", url, true)
	xhttp.send()

}
