import React, {Component} from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import DoneIcon from 'material-ui/svg-icons/action/done'
import autobind from 'autobind-decorator'

const formStyle = {
	padding: 20,
	display: 'flex',
	alignItems: 'center',
	flexDirection: 'column'
}
const buttonStyle = {
	marginTop: 20,
	marginBottom: 20,
	width: 256
}

@autobind
export default class ItemForm extends Component {

	constructor(props) {
		super(props)

		let item = props.item
		if (!item) {
			item = {}
			for (let col of props.colsConfig) {
				item[col.attrName] = ''
			}
		}

		this.state = {
			...props,
			item: item
		}
	}

	_handleChange(event, date) {
		const item = {
			...this.state.item
		}
		item[event.target.id] = event.target.value
		this.setState({item: item})
	}

	_handleValidate() {
		this.props.handleAddit(this.state.item)
	}

	render() {
		const {colsConfig, item} = this.state
		return (
			<div style={formStyle}>
				{colsConfig.map(col => {
					if (col.container) {
						const AutoComp = col.container
						return (
							<AutoComp
								value={item[col.attrName]}
								handleChange={this._handleChange}
								key={col.attrName}
							/>
						)
					} else {
						return (
							<TextField
								floatingLabelFixed={true}
								hintText={(item[col.attrName] === '')? col.tooltip :''}
								floatingLabelText={col.unit? col.colName+' ('+col.unit+')' :col.colName}
								id={col.attrName}
								value={item[col.attrName]}
								key={col.attrName}
								onChange={this._handleChange}
							/>
						)
					}
				})}
				<RaisedButton
					style={buttonStyle}
					label="Valider"
					secondary={true}
					icon={< DoneIcon />}
					onTouchTap={this._handleValidate}
				/>
			</div>
		)
	}
}
