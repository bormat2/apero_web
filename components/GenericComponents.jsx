import React from 'react'
import {Link} from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'
import AutoComplete from 'material-ui/AutoComplete'
import AddIcon from 'material-ui/svg-icons/social/person-add'
import DeleteIcon from 'material-ui/svg-icons/content/delete-sweep'
import EditIcon from 'material-ui/svg-icons/editor/border-color'
import Drawer from 'material-ui/Drawer'
import {
	Toolbar,
	ToolbarGroup,
	ToolbarSeparator,
	ToolbarTitle
} from 'material-ui/Toolbar'

export const EditToolBar = (props) => (
	<Toolbar>
		<ToolbarGroup >
			<ToolbarTitle text={props.title}/>

			<ToolbarSeparator/>
		</ToolbarGroup>
		<ToolbarGroup style={{marginLeft: 'auto'}}>
			{(!props.title.includes('Exemplaires') && !props.title.includes('Panier') && !props.title.includes('Factures'))?
				<RaisedButton
					label="Nouveau"
					primary={true}
					icon={< AddIcon />}
					onTouchTap={props.handleNew}
				/>
			:null}
			{(!props.title.includes('Exemplaires disponibles') && !props.title.includes('vendus') && !props.title.includes('Factures'))?
				<RaisedButton
					label="Supprimer"
					secondary={true}
					icon={< DeleteIcon />}
					onTouchTap={props.handleDelete}
				/>
			:null}
		</ToolbarGroup>
	</Toolbar>
)

export const FormDrawer = (props) => (
	<Drawer
		width={props.width || 500}
		open={props.open}
		docked={false}
		onRequestChange={props.handleChange}
		openSecondary={true}
	>
		{props.form}
	</Drawer>
)

export const InputGroup = (props) => (
	<div style={{
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	}}
	>
		<AutoComplete
			floatingLabelText={props.label}
			hintText={props.hint}
			searchText={props.searchText}
			onUpdateInput={props.handleUpdateInput}
			dataSource={props.data}
			filter={(searchText, key) => (key.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)}
			openOnFocus={true}
			fullWidth={true}
			style={{
				maxWidth: 500,
				minWidth: 256
			}}
		/>
		{props.button?
			<Link to={props.href}>
				<RaisedButton
					label={"gérer les "+props.label+"s"}
					secondary={true}
					icon={< EditIcon />}
					style={{
						width: 320
					}}
				/>
			</Link>
		:null}
	</div>
)
