import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as AdherentActions from '../actions/adherents'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import autobind from 'autobind-decorator'

@autobind
class Entity extends Component {

	constructor(props) {
		super(props)
		this.state = {
			openForm: false,
			editingIndex: 0
		}
	}

	handleSort(sorting) {
		this.sortAttr = sorting.sortAttr
		this.ascendent = sorting.ascendent
		console.log(this.sortAttr, this.ascendent)
	}

	_handleSelect(selectedRows) {
		if (selectedRows === 'all') {
			const rows = Object.keys(this.props.data.items)
			this
				.props
				.actions
				.select(rows)
			this._propagateSelect(rows)
			return
		}
		if (selectedRows === 'none') {
			this._deselectAll()
			return
		}
		this
			.props
			.actions
			.select(selectedRows)
		this._propagateSelect(selectedRows)
	}

	_propagateSelect(rows) {
		if (this.props.handleSelect) {
			if (!this.sortAttr) {
				this.sortAttr = this.colsConfig[0].attrName
				this.ascendent = 1
			}
			let sortedItems = this.props.data
				.items
				.sort((a, b) => {
					const aa = a[this.sortAttr]
					const bb = b[this.sortAttr]
					return aa.localeCompare(bb) * this.ascendent
				})
			if (this.props.filterOut) {
				const ids = []
				for (let item of this.props.filterOut.items) {
					ids.push(item.id)
				}
				for (let id of ids) {
					sortedItems = sortedItems.filter(exemplaire => {
						return +exemplaire.id !== +id
					})
				}
			}
			const items = []
			for (let row of rows) {
				items.push(sortedItems[row])
			}
			this.props.handleSelect(items)
		}
	}

	_deselectAll() {
		this
			.props
			.actions
			.select([])
	}

	_deleteSelected() {
		for (let index of this.props.data.selectedRows) {
			let id = this.props.data.items[index].id
			if (this.props.famille_id) {
				id = this.filtered[index].id
			}
			this
				.props
				.actions
				.del(id)
		}
		this._deselectAll()
	}

	_handleToggleForm() {
		this.setState({
			openForm: !this.state.openForm
		})
	}

	_handleEdit(index) {
		this._handleToggleForm()
		this.setState({editingIndex: index})
	}

	_handleAddit(data) {

		let _data = {
			...data
		}
		if (this.props.famille_id) {
			_data = {
				..._data,
				id_famille: this.props.famille_id
			}
		}

		this._handleToggleForm()
		this
			.props
			.actions
			.addit(_data)
	}

	_render() {

		const data = {...this.props.data}
		if (this.props.adherent_id) {
			data.items = data.items.filter(exemplaire => {
				return +exemplaire.id_adherent === +this.props.adherent_id
			})
			this.filtered = data.items
		}
		if (this.props.ouvrage_id) {
			data.items = data.items.filter(exemplaire => {
				return +exemplaire.id_ouvrage === +this.props.ouvrage_id
			})
			this.filtered = data.items
		}
		if (this.props.famille_id) {
			data.items = data.items.filter(enfant => {
				return +enfant.id_famille === +this.props.famille_id
			})
			this.filtered = data.items
		}
		if (this.props.handleSelect) {
			data.items = data.items.filter(e => (!e.n_facture))
			const ids = []
			for (let item of this.props.filterOut.items) {
				ids.push(item.id)
			}
			for (let id of ids) {
				data.items = data.items.filter(e => (+e.id !== +id))
			}
			this.filtered = data.items
		}

		if (this.props.filter) {
			if (this.props.filter === 'vendus') {
				data.items = data.items.filter(exemp => {
					return !!exemp.n_facture
				})
			}
			if (this.props.filter === 'invendus') {
				data.items = data.items.filter(exemp => {
					return !exemp.n_facture
				})
			}
			this.filtered = data.items
		}

		return (
			<div style={{width: '100%'}}>
				<EditToolBar
					title={this.title}
					handleDelete={this._deleteSelected}
					handleNew={this._handleEdit}
				/>
				<TableUI handleSelect={this._handleSelect}
					handleEdit={this._handleEdit}
					colsConfig={this.colsConfig}
					data={data}
					handleSort={this.handleSort}
				/>
			</div>
		)
	}
}

export default Entity
