import React, {Component} from "react"
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer'
import {List, ListItem} from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import StorageIcon from 'material-ui/svg-icons/device/storage'
import ShoppingCartIcon from 'material-ui/svg-icons/action/shopping-cart'
import PeopleIcon from 'material-ui/svg-icons/social/people'
import SettingsIcon from 'material-ui/svg-icons/action/settings'
import PrintIcon from 'material-ui/svg-icons/action/print'
import MoneyIcon from 'material-ui/svg-icons/editor/attach-money'
import NoMoneyIcon from 'material-ui/svg-icons/editor/money-off'
import BookIcon from 'material-ui/svg-icons/communication/import-contacts'
import ReceiptIcon from 'material-ui/svg-icons/action/receipt'
import TuneIcon from 'material-ui/svg-icons/av/explicit'
import SchoolIcon from 'material-ui/svg-icons/social/school'
import BusinessIcon from 'material-ui/svg-icons/communication/business'
import ContentPasteIcon from 'material-ui/svg-icons/content/content-paste'
import WorkIcon from 'material-ui/svg-icons/action/work'
import ModeEditIcon from 'material-ui/svg-icons/editor/mode-edit'
import autobind from 'autobind-decorator'

const defaultStyle = {
	paddingLeft: 20,
	userSelect: 'none'
}

@autobind
export default class Header extends Component {

	constructor() {
		super()
		this.state = {
			open: false
		}
	}

	_toggleMenu() {
		this.setState({open: !this.state.open})
	}

	render() {
		const action = this._toggleMenu
		let i = 0
		return (
			<header className="header">
				<AppBar
					title={"APERO - Bourse aux livres"}
					onLeftIconButtonTouchTap={action}
				/>
				<Drawer
					open={this.state.open}
					docked={false}
					onRequestChange={action}
				>

					<List>
						<Subheader>Gestion de la bourse aux livres</Subheader>

						<RouteItem
							keyy={i++}
							action={action}
						/>
						<RouteItem
							keyy={i++}
							action={action}
						/>
						<RouteItem
							keyy={i++}
							action={action}
						/>
						<ListItem
							primaryText="Administration"
							leftIcon={<PrintIcon />}
							primaryTogglesNestedList={true}
							initiallyOpen={true}
							className='main-menu-link'
							nestedItems={[
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>
							]}
						/>
						<ListItem
							primaryText="Imprimer"
							leftIcon={<PrintIcon />}
							primaryTogglesNestedList={true}
							initiallyOpen={false}
							className='main-menu-link'
							nestedItems={[
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>,
								<RouteItem
									keyy={i++}
									key={i}
									action={action}
								/>
							]}
						/>
					</List>
				</Drawer>
			</header>
		)
	}
}

const items = [
	{
		to: "depots",
		name: "Dépots",
		icon: <StorageIcon />
	}, {
		to: "ventes",
		name: "Ventes",
		icon: <ShoppingCartIcon />
	}, {
		to: "adherents",
		name: "Adhérents",
		icon: <PeopleIcon />
	}, {
		to: "ouvrages",
		name: "Ouvrages",
		icon: <BookIcon />
	}, {
		to: "editeurs",
		name: "Éditeurs",
		icon: <ModeEditIcon />
	}, {
		to: "classes",
		name: "Classes",
		icon: <SchoolIcon />
	}, {
		to: "sections",
		name: "Sections",
		icon: <WorkIcon />
	}, {
		to: "matieres",
		name: "Matières",
		icon: <ContentPasteIcon />
	}, {
		to: "etabs",
		name: "Établissments",
		icon: <BusinessIcon />
	}, {
		to: "etats",
		name: "États",
		icon: <TuneIcon />
	}, {
		to: "factures",
		name: 'Factures',
		icon: <ReceiptIcon/>
	}, {
		to: "vendus",
		name: "Vendus",
		icon: <MoneyIcon />
	}, {
		to: "invendus",
		name: "Invendus",
		icon: <NoMoneyIcon />
	}

]

const RouteItem = (props) => (
	<Link to={items[props.keyy].to} className='main-menu-link'>
		<ListItem
			primaryText={items[props.keyy].name}
			leftIcon={items[props.keyy].icon}
			onTouchTap={props.action}
			nestedLevel={props.nestedLevel}
		/>
	</Link>
)
