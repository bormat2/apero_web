import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {
	Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import moment from 'moment'

const headerItemStyle = {
	cursor: 'pointer',
	userSelect: 'none'
}
const cellStyle = {
	height: '100%',
	width: '100%',
	display: 'flex',
	alignItems: 'center',
	marginLeft: '-24px',
	paddingLeft: '24px',
	marginRight: '-24px',
	paddingRight: '24px'
}

export default class TableUI extends Component {

	static get propTypes() {
		return {
			colsConfig: PropTypes.array.isRequired,
			handleSelect: PropTypes.func.isRequired,
			handleEdit: PropTypes.func.isRequired,
			handleSort: PropTypes.func.isRequired,
			data: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)
		this.state = {
			sortingAttr: props.colsConfig[0].attrName,
			ascendent: 1
		}
	}

	_handleSort(newSortAttr) {
		const newState = (this.state.sortingAttr === newSortAttr)
			? {
				ascendent: -(this.state.ascendent)
			}
			: {
				sortingAttr: newSortAttr,
				ascendent: 1
			}
		this.setState(newState)
		this.props.handleSort(newState)
	}

	render() {
		const {
			colsConfig,
			handleSelect,
			handleEdit,
			data
		} = this.props
		const selectedRows = data.selectedRows
		const items = data
			.items
			.sort((a, b) => {
				const aa = a[this.state.sortingAttr]
				const bb = b[this.state.sortingAttr]
				return aa.localeCompare(bb) * this.state.ascendent
			})
		if (items.length === 0) { return (
			<Paper style={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				justiyContent: 'center',
				marginTop: 12,
				width: '100%'
			}}>
				<h3>Auncun élément à afficher dans cette liste</h3>
			</Paper>
		)}
		return (
			<Table multiSelectable={true} onRowSelection={handleSelect}>
				<TableHeader adjustForCheckbox={true}>
					<TableRow>
						{colsConfig.map((c, i) => (
							<TableHeaderColumn tooltip={c.tooltip}
								onTouchTap={() => this._handleSort(c.attrName)}
								style={headerItemStyle}
								key={i}>
								{c.colName}
							</TableHeaderColumn>
						))}
					</TableRow>
				</TableHeader>

				<TableBody showRowHover={true}>
					{items.map((row, index) => (
						<TableRow key={index} selected={selectedRows.indexOf(index) >= 0}>
							{colsConfig.map((c, i) => {
								let value = row[c.attrName]
								if (c.type === 'date' && moment(value).isValid()) {
									value = moment(value).format('DD/MM/YYYY')
								}
								if (c.unit) {
									value +=' '+c.unit
								}
								const NameContainer = c.container
								return (
									<TableRowColumn key={i}>
										<div
											style={cellStyle}
											onClick={e => {
												e.stopPropagation()
												handleEdit(index)
												if (colsConfig[0].attrName === 'n_facture') {
													window.open('fact.php?n_facture='+row.n_facture)
												}
											}}
										>{NameContainer?
											<NameContainer id={value}/>
										:value}</div>
									</TableRowColumn>
								)
							})}
						</TableRow>
					))}
				</TableBody>
			</Table>
		)
	}
}
