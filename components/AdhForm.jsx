import React, {Component} from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import DoneIcon from 'material-ui/svg-icons/action/done'
import DatePicker from 'material-ui/DatePicker'
import moment from 'moment'
import areIntlLocalesSupported from 'intl-locales-supported'
import autobind from 'autobind-decorator'
import Children from '../containers/Children'

const formStyle = {
	paddingTop: 20,
	paddingBottom: 20,
	display: 'flex',
	alignItems: 'center',
	flexDirection: 'column'
}
const buttonStyle = {
	marginTop: 20,
	marginBottom: 20,
	width: 256
}

let DateTimeFormat
if (areIntlLocalesSupported(['fr', 'fa-IR'])) {
	DateTimeFormat = global.Intl.DateTimeFormat
} else {
	const IntlPolyfill = require('intl')
	DateTimeFormat = IntlPolyfill.DateTimeFormat
	require('intl/locale-data/jsonp/fr')
	require('intl/locale-data/jsonp/fa-IR')
}

@autobind
export default class AdhForm extends Component {

	constructor(props) {
		super(props)

		let adherent = props.adherent
		if (!adherent) {
			adherent = {}
			for (let col of props.colsConfig) {
				adherent[col.attrName] = ''
			}
			adherent.date_derniere_adhesion = moment().format('YYYY-MM-DD hh:mm:ss')
		}

		this.state = {
			...props,
			adherent: adherent
		}
	}

	_handleChange(event, date) {
		const adherent = {
			...this.state.adherent
		}
		if (date instanceof Date) {
			const d = moment(date).format('YYYY-MM-DD hh:mm:ss')
			adherent.date_derniere_adhesion = d
		} else {
			adherent[event.target.id] = event.target.value
		}
		this.setState({adherent: adherent})
	}

	_handleValidate() {
		this.props.handleAddit(this.state.adherent)
	}

	render() {
		const {colsConfig, adherent} = this.state
		return (
			<div style={formStyle}>
				{colsConfig.map(col => {
					if (!col.type || !moment(adherent[col.attrName]).isValid()) {
						return (
							<TextField
								floatingLabelFixed={true}
								hintText={(adherent[col.attrName] === '')? col.tooltip :''}
								floatingLabelText={col.colName}
								id={col.attrName}
								value={adherent[col.attrName]}
								key={col.attrName}
								onChange={this._handleChange}
							/>
						)
					} else if (col.type === 'date') {
						return (
							<DatePicker
								id={col.attrName}
								key={col.attrName}
								floatingLabelText={col.colName}
								hintText={(adherent[col.attrName] === '')? col.tooltip :''}
								value={moment(adherent[col.attrName]).toDate()}
								onChange={this._handleChange}
								DateTimeFormat={DateTimeFormat}
								okLabel="OK"
								cancelLabel="Annuler"
								locale="fr"
							/>
						)
					}
				})}
				<RaisedButton
					style={buttonStyle}
					label="Valider"
					secondary={true}
					icon={< DoneIcon />}
					onTouchTap={this._handleValidate}
				/>
				{adherent.id?
					<Children famille_id={adherent.id}/>
				:null}
			</div>
		)
	}
}
