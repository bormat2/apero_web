-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2017 at 03:57 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apero_2`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `classes_json` ()  select CONCAT_WS('','{"decode":["id","lib","id_etabliss","id_section"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.lib,c.id_etabliss,c.id_section),'"]') as strf
	FROM classe AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `classe_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from classe WHERE classe.id = @id;
    IF is_update < 1 then
		insert into classe() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update classe
	SET classe.lib = @lib,
		classe.id_etabliss = @id_etabliss,
		classe.id_section = @id_section
	WHERE classe.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_classe` ()  DELETE FROM classe WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_editeur` ()  DELETE FROM editeur WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_enfant` ()  DELETE FROM enfant WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_etablissement` ()  DELETE FROM etablissement WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_etat` ()  DELETE FROM etat WHERE id=@id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_exemplaire` ()  DELETE FROM exemplaire WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_famille_adherente` ()  DELETE FROM famille_adherente WHERE id=@id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_matiere` ()  DELETE FROM matiere WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_ouvrage` ()  DELETE FROM ouvrage WHERE id = @id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_vente` ()  DELETE FROM vente WHERE id=@id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editeur_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from editeur WHERE editeur.id = @id;
    IF is_update < 1 then
		insert into editeur() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update editeur
	SET editeur.nom = @nom
	WHERE editeur.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editeur_json` ()  select CONCAT_WS('','{"decode":["id","nom"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.nom),'"]') as strf
	FROM editeur AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `enfant_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from enfant WHERE enfant.id = @id;
    IF is_update < 1 then
		insert into enfant() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update enfant
	SET enfant.nom = @nom,
	enfant.prenom = @prenom,
	enfant.id_famille = @id_famille,
	enfant.id_classe = @id_classe
	WHERE enfant.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `enfant_json` ()  select CONCAT_WS('','{"decode":["id","nom","prenom","id_famille","id_classe"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',e.id,e.nom,e.prenom,e.id_famille,e.id_classe),'"]') as strf
	FROM enfant AS e
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `etablissement_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from etablissement WHERE etablissement.id = @id;
    IF is_update < 1 then
		insert into etablissement() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update etablissement
	SET etablissement.nom = @nom,
		etablissement.tel = @tel
	WHERE etablissement.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `etab_json` ()  select CONCAT_WS('','{"decode":["id","nom","tel"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.nom,c.tel),'"]') as strf
	FROM etablissement AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `etat_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from etat WHERE etat.id = @id;
    IF is_update < 1 then
		insert into etat() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update etat
	SET etat.lib = @lib,
		etat.decote = @decote
	WHERE etat.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `etat_json` ()  select CONCAT_WS('','{"decode":["id","lib","decote"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.lib,c.decote),'"]') as strf
	FROM etat AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `exemplaire_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from exemplaire WHERE exemplaire.id = @id;
    IF is_update < 1 then
		insert into exemplaire() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update exemplaire
	SET exemplaire.id_ouvrage = @id_ouvrage,
		exemplaire.id_etat = @id_etat,
		exemplaire.id_adherent = @id_adherent,
		exemplaire.n_facture = @n_facture
	WHERE exemplaire.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `exemplaire_depot_by_famille_json` ()  select CONCAT_WS('','{"decode":["id",{"exemplaire":["id"]}]',
	'\n,"data":[',
	GROUP_CONCAT('[',strF,']' SEPARATOR ',\n'),']}') as json_str from (
	SELECT CONCAT(f.id,',[',GROUP_CONCAT(e.id SEPARATOR ','),']') as strF
	FROM famille_adherente AS f
	inner join exemplaire e on e.id_adherent = f.id
	group by f.id
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `exemplaire_json` ()  select CONCAT_WS('','{"decode":["id","id_ouvrage","id_etat","id_adherent","n_facture"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',id, id_ouvrage, id_etat, id_adherent, n_facture),'"]') as strf
	FROM exemplaire AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `factt` ()  select f.id
	FROM exemplaire AS f$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `factures` ()  READS SQL DATA
select f.*	FROM exemplaire AS f$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `facture_addit` ()  BEGIN
    select ex.*
    from exemplaire ex;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `famille_adherente_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from famille_adherente WHERE famille_adherente.id = @id;
	IF is_update < 1 then
		insert into famille_adherente() values ();
		SET @id := LAST_INSERT_ID();
	END IF ;
	Update famille_adherente
	SET
	famille_adherente.nom_resp_legal = @nom_resp_legal,
	famille_adherente.prenom_resp_legal = @prenom_resp_legal,
	famille_adherente.date_derniere_adhesion = @date_derniere_adhesion,
	famille_adherente.adresse = @adresse,
	famille_adherente.ville = @ville
	WHERE famille_adherente.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `famille_adherente_json` ()  select CONCAT_WS('','{"decode":["id","nom_resp_legal","prenom_resp_legal","date_derniere_adhesion","adresse","ville",{"children":["id","nom","prenom","class"]}]',
	'\n,"data":[',
	GROUP_CONCAT(conct.strf SEPARATOR ',\n'),']}') as json_str from (
	SELECT CONCAT('[',
		CONCAT_WS('',f.id,',"',f.nom_resp_legal,'","',f.prenom_resp_legal,'","',f.date_derniere_adhesion,'","',
			f.adresse,'","',f.ville),
		'",[',
			GROUP_CONCAT(
				IF(e.id IS NULL,'',CONCAT('[',e.id,',"',e.nom,'","',e.prenom,'","',e.id_classe,'"]')) SEPARATOR ','
			),
		']]\n\n'
	) as strF
	FROM famille_adherente AS f
	left JOIN enfant e on e.id_famille = f.id
	group by f.id
) as conct$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `livre_achat_by_famille_json` ()  select CONCAT_WS('','{"decode":["id",{"exemplaire":["id"]}]',
	'\n,"data":[',
	GROUP_CONCAT('[',strF,']' SEPARATOR ',\n'),']}') as json_str from (
	SELECT CONCAT(f.id,',[',GROUP_CONCAT(e.id SEPARATOR ','),']') as strF
	FROM famille_adherente f
	INNER JOIN vente ON f.id = vente.id_adherent
	INNER JOIN exemplaire e ON vente.n_facture = e.n_facture
	group by f.id
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `matiere_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from matiere WHERE matiere.id = @id;
    IF is_update < 1 then
		insert into matiere() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update matiere
	SET matiere.nom = @nom
	WHERE matiere.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `matiere_json` ()  select CONCAT_WS('','{"decode":["id","nom"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.nom),'"]') as strf
	FROM matiere AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ouvrage_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from ouvrage WHERE ouvrage.id = @id;
    IF is_update < 1 then
		insert into ouvrage() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update ouvrage
	SET ouvrage.nom = @nom,
		ouvrage.annee = @annee,
		ouvrage.id_matiere = @id_matiere,
		ouvrage.prix_neuf = @prix_neuf,
		ouvrage.id_editeur = @id_editeur
	WHERE ouvrage.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ouvrage_json` ()  select CONCAT_WS('','{"decode":["id","nom","annee","id_matiere","prix_neuf","id_editeur"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',id,nom,annee,id_matiere,prix_neuf,id_editeur),'"]') as strf
	FROM ouvrage AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `section_addit` ()  BEGIN
	DECLARE is_update INT;
    select count(*) INTO is_update from section WHERE section.id = @id;
    IF is_update < 1 then
		insert into section() values ();
		SET @id := LAST_INSERT_ID();
	END IF;
	Update section
	SET section.lib = @lib
	WHERE section.id = @id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `section_json` ()  select CONCAT_WS('','{"decode":["id","lib"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',c.id,c.lib),'"]') as strf
	FROM section AS c
) as useless$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `totoy` ()  select * FROM exemplaire AS f$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `vente` ()  BEGIN
    DECLARE v_cur_position INT;DECLARE v_remainder TEXT;DECLARE LID INT;DECLARE v_cur_string VARCHAR(255);SET v_cur_position = 1;
    SET v_remainder = @exemplaires;
	insert into vente(id_adherent,date_vente) values (@id_adherent,Now());
	SET LID = LAST_INSERT_ID();
    WHILE CHAR_LENGTH(v_remainder) > 0 AND v_cur_position > 0 DO
     SET v_cur_position = INSTR(v_remainder, '_');IF v_cur_position = 0 THEN SET v_cur_string = v_remainder;ELSE SET v_cur_string = LEFT(v_remainder, v_cur_position - 1);END IF;
        SET v_remainder = SUBSTRING(v_remainder, v_cur_position + 1);

        Update exemplaire
		SET
		exemplaire.n_facture = LID
		WHERE exemplaire.id = v_cur_string;

    END WHILE;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `classe`
--

CREATE TABLE `classe` (
  `id` int(10) NOT NULL,
  `lib` varchar(255) DEFAULT NULL,
  `id_etabliss` int(11) DEFAULT NULL,
  `id_section` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classe`
--

INSERT INTO `classe` (`id`, `lib`, `id_etabliss`, `id_section`) VALUES
(2, '301', 11, 6),
(3, '302', 11, 9),
(4, '400', 9, 8),
(5, '4056', 10, 7),
(6, '543', 10, 6),
(7, '431', 12, 7);

-- --------------------------------------------------------

--
-- Table structure for table `demande`
--

CREATE TABLE `demande` (
  `id_ouvrage` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `editeur`
--

CREATE TABLE `editeur` (
  `id` int(10) NOT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editeur`
--

INSERT INTO `editeur` (`id`, `nom`) VALUES
(13, 'Larrousse'),
(14, 'LaBlonde'),
(15, 'LaBrune');

-- --------------------------------------------------------

--
-- Table structure for table `enfant`
--

CREATE TABLE `enfant` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `id_famille` int(11) NOT NULL,
  `id_classe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enfant`
--

INSERT INTO `enfant` (`id`, `nom`, `prenom`, `id_famille`, `id_classe`) VALUES
(9, 'Lacaze', 'charlie junior', 15, 4),
(10, 'Lacaze', 'charlotte junior', 15, 6),
(11, 'Bortolaso', 'mathieu junior', 16, 2),
(12, 'Bortolaso', 'sarah junior', 16, 7),
(13, 'Horvat', 'Rafiki', 17, 5),
(14, 'Horvat', 'Clément', 17, 6),
(15, 'Campana', 'France', 18, 2),
(16, 'Venturi', 'Florian', 18, 3);

-- --------------------------------------------------------

--
-- Table structure for table `etablissement`
--

CREATE TABLE `etablissement` (
  `id` int(10) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `etablissement`
--

INSERT INTO `etablissement` (`id`, `nom`, `tel`) VALUES
(9, 'Collège Lakanal', '0561808989'),
(10, 'Lycée Gabriel Fauré', '0563458956'),
(11, 'collège Jules Verne', '0567879045'),
(12, 'Lycée Déodat De Severac', '0598345678');

-- --------------------------------------------------------

--
-- Table structure for table `etat`
--

CREATE TABLE `etat` (
  `id` int(10) NOT NULL,
  `lib` varchar(255) DEFAULT NULL,
  `decote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `etat`
--

INSERT INTO `etat` (`id`, `lib`, `decote`) VALUES
(9, 'Très Bon état', 25),
(10, 'Bon état', 40),
(11, 'Assez Bon état', 50),
(12, 'Endommagé', 65);

-- --------------------------------------------------------

--
-- Table structure for table `exemplaire`
--

CREATE TABLE `exemplaire` (
  `id` int(10) NOT NULL,
  `n_facture` int(11) DEFAULT NULL,
  `id_etat` int(11) DEFAULT NULL,
  `id_adherent` int(11) DEFAULT NULL,
  `id_ouvrage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exemplaire`
--

INSERT INTO `exemplaire` (`id`, `n_facture`, `id_etat`, `id_adherent`, `id_ouvrage`) VALUES
(1, 5, 10, 15, 20);

-- --------------------------------------------------------

--
-- Table structure for table `famille_adherente`
--

CREATE TABLE `famille_adherente` (
  `id` int(10) NOT NULL,
  `nom_resp_legal` varchar(255) DEFAULT NULL,
  `prenom_resp_legal` varchar(255) DEFAULT NULL,
  `date_derniere_adhesion` datetime DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `toString` varchar(243) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `famille_adherente`
--

INSERT INTO `famille_adherente` (`id`, `nom_resp_legal`, `prenom_resp_legal`, `date_derniere_adhesion`, `adresse`, `ville`, `toString`) VALUES
(15, 'Lacaze', 'Charlotte', '2016-07-20 00:00:00', '15 rue grandet', 'Rodez', 'Lacaze charlotte - 15 rue grandet'),
(16, 'Bortolaso', 'Mathieu', '2016-07-25 00:00:00', '5 impasse du comte de foix', 'Plaisance du touch', 'Bortolaso Mathieu - 5 impasse du comte de foix'),
(17, 'Horvat', 'Rafael', '2016-07-20 00:00:00', '15 rue grandet', 'Rodez', 'Horvat Rafael - 15 rue grandet'),
(19, '- - -Non Adhérent', '-', NULL, '-', '-', '- - -Non Adhérent - - -');

-- --------------------------------------------------------

--
-- Table structure for table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(10) NOT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matiere`
--

INSERT INTO `matiere` (`id`, `nom`) VALUES
(18, 'Mathématiques'),
(19, 'Français'),
(20, 'Histoire'),
(21, 'Chinois'),
(22, 'Géographie'),
(23, 'Physique'),
(24, 'SVT'),
(25, 'RAD');

-- --------------------------------------------------------

--
-- Table structure for table `ouvrage`
--

CREATE TABLE `ouvrage` (
  `id` int(10) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `annee` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  `prix_neuf` int(11) DEFAULT NULL,
  `id_editeur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `nom`, `annee`, `id_matiere`, `prix_neuf`, `id_editeur`) VALUES
(20, 'Français 3ème', 2016, 19, 23, 13),
(21, 'Antigone', 2008, 19, 12, 15),
(22, 'Latin 3e : Langue & Culture', 2012, 19, 32, 15),
(23, 'Join the Team 3e', 2013, 21, 45, 15),
(24, 'Join the Team 3e : Workbook', 2013, 21, 90, 15),
(25, 'Histoire-geographie 3e', 2012, 20, 40, 13),
(26, 'Sciences de la vie de la terre, 3e', 2012, 24, 33, 14),
(27, 'Math 3e (programme 2012)', 2012, 18, 67, 14);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(10) NOT NULL,
  `lib` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `lib`) VALUES
(6, 'Littéraire'),
(7, 'Scientifique'),
(8, 'Théatre'),
(9, 'STG');

-- --------------------------------------------------------

--
-- Table structure for table `vente`
--

CREATE TABLE `vente` (
  `n_facture` int(10) NOT NULL,
  `date_vente` datetime NOT NULL,
  `id_adherent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vente`
--

INSERT INTO `vente` (`n_facture`, `date_vente`, `id_adherent`) VALUES
(56, '2016-07-03 19:33:20', 19),
(57, '2016-07-03 20:14:48', 15),
(62, '2016-07-03 20:23:10', 17),
(63, '2016-07-03 20:24:34', 17);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id_ouvrage`,`id_classe`);

--
-- Indexes for table `editeur`
--
ALTER TABLE `editeur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enfant`
--
ALTER TABLE `enfant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exemplaire`
--
ALTER TABLE `exemplaire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `famille_adherente`
--
ALTER TABLE `famille_adherente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ouvrage`
--
ALTER TABLE `ouvrage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vente`
--
ALTER TABLE `vente`
  ADD PRIMARY KEY (`n_facture`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `editeur`
--
ALTER TABLE `editeur`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `etat`
--
ALTER TABLE `etat`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `exemplaire`
--
ALTER TABLE `exemplaire`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `famille_adherente`
--
ALTER TABLE `famille_adherente`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `ouvrage`
--
ALTER TABLE `ouvrage`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vente`
--
ALTER TABLE `vente`
  MODIFY `n_facture` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE OR replace PROCEDURE vente_json()
select CONCAT_WS('','{"decode":["n_facture","date_vente","id_adherent"],"data":[',GROUP_CONCAT(strf SEPARATOR ','),']}')
from (
    select concat('["',concat_ws('","',n_facture, date_vente, id_adherent),'"]') as strf
	FROM vente AS c
) as useless;
