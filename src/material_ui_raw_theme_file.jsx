import {fade} from 'material-ui/utils/colorManipulator'
import * as Colors from 'material-ui/styles/colors'
import {spacing, getMuiTheme} from 'material-ui/styles'

const rawBaseTheme = {
	...spacing,
	fontFamily: 'Roboto, sans-serif',
	palette: {
		primary1Color: Colors.tealA400,
		primary2Color: Colors.tealA700,
		primary3Color: Colors.teal900,
		accent1Color: Colors.greenA100,
		accent2Color: Colors.grey100,
		accent3Color: Colors.grey500,
		textColor: Colors.teal800,
		alternateTextColor: Colors.teal900,
		canvasColor: Colors.white,
		borderColor: Colors.grey300,
		disabledColor: fade(Colors.grey800, 0.3)
	},
	raisedButton: {
		disabledColor: Colors.teal600,
		disabledTextColor: Colors.teal900
	}
}

//Theme must be wrapped in funciton getMuiTheme
export default getMuiTheme(rawBaseTheme)
