import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import ExemplaireActions from '../actions/exemplaires'
import OuvrageActions from '../actions/ouvrages'
import EtatActions from '../actions/etats'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import OuvragesName from './OuvragesName'
import EtatsName from './EtatsName'
import autobind from 'autobind-decorator'

@autobind
class Exemplaires extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired,
			ouvrActions: PropTypes.object.isRequired,
			etatActions: PropTypes.object.isRequired,
			adherent_id: PropTypes.number,
			ouvrage_id: PropTypes.string,
			handleSelect: PropTypes.func,
			filterOut: PropTypes.object,
			filter: PropTypes.string
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = props.handleSelect? 'Exemplaires disponibles': 'Exemplaires déposés'
		if (props.filter) {
			this.title = 'Exemplaires '+props.filter
		}

		setImmediate(() => {
			props
				.actions
				.fetch()
			props
				.ouvrActions
				.fetch()
			props
				.etatActions
				.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.exemplaires}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ExemplaireActions, dispatch),
		ouvrActions: bindActionCreators(OuvrageActions, dispatch),
		etatActions: bindActionCreators(EtatActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Exemplaires)

const colsConfig = [
	{
		colName: 'Ouvrage',
		tooltip: "Le nom de l'ouvrage",
		attrName: 'id_ouvrage',
		container: OuvragesName
	}, {
		colName: 'État',
		tooltip: "L'état de l'ouvrage",
		attrName: 'id_etat',
		container: EtatsName
	}
]
