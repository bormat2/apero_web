import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import ClasseActions from '../actions/classes'
import SectionsActions from '../actions/sections'
import EtabActions from '../actions/etabs'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import autobind from 'autobind-decorator'
import EtabsName from './EtabsName'
import SectionsName from './SectionsName'

@autobind
class Classes extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired,
			sectionActions: PropTypes.object.isRequired,
			etabsActions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Classes'

		setImmediate(() => {
			props.actions.fetch()
			props.sectionActions.fetch()
			props.etabsActions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}

				<FormDrawer
					form={this.state.openForm?
						<ItemForm
							colsConfig={colsConfig}
							item={this.props.data.items[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.classes}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ClasseActions, dispatch),
		sectionActions: bindActionCreators(SectionsActions, dispatch),
		etabsActions: bindActionCreators(EtabActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Classes)

const colsConfig = [
	{
		colName: 'Libellé',
		tooltip: "Le nom ou le numéro de la classe",
		attrName: 'lib'
	}, {
		colName: 'Établissement',
		tooltip: "L'établissement de la classe",
		attrName: 'id_etabliss',
		container: EtabsName
	}, {
		colName: 'Section',
		tooltip: "La section de la classe",
		attrName: 'id_section',
		container: SectionsName
	}
]
