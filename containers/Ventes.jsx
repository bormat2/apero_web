import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import AdherentActions from '../actions/adherents'
import OuvrageActions from '../actions/ouvrages'
import EtatActions from '../actions/etats'
import ExemplaireActions from '../actions/exemplaires'
import ExemplairesPanierActions from '../actions/exemplaires_panier'
import Exemplaires from './Exemplaires'
import ExemplairesPanier from './ExemplairesPanier'
import RaisedButton from 'material-ui/RaisedButton'
import CartPlusIcon from 'material-ui/svg-icons/action/add-shopping-cart'
import CartIcon from 'material-ui/svg-icons/action/shopping-cart'
import autobind from 'autobind-decorator'
import {InputGroup} from '../components/GenericComponents'

@autobind
class Ventes extends Component {

	static get propTypes() {
		return {
			adherents: PropTypes.object.isRequired,
			ouvrages: PropTypes.object.isRequired,
			exemplaires: PropTypes.object.isRequired,
			adhActions: PropTypes.object.isRequired,
			ouvrActions: PropTypes.object.isRequired,
			etatsActions: PropTypes.object.isRequired,
			exempActions: PropTypes.object.isRequired,
			exmpPanierActions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.state = {
			adhSearchText: '',
			ouvrSearchText: '',
			selected: []
		}

		setImmediate(() => {
			props
				.adhActions
				.fetch()
			props
				.ouvrActions
				.fetch()
			props
				.etatsActions
				.fetch()
		})
	}

	handleUpdateInputAdh(searchText, dataSource) {
		this.setState({adhSearchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({id_adherent: id})
	}

	handleUpdateInputOuvr(searchText, dataSource) {
		this.setState({ouvrSearchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({id_ouvrage: id})
	}

	handleSelect(items) {
		this.setState({
			selected: items
		})
	}

	handlePanier() {
		for (let item of this.state.selected) {
			this
				.props
				.exmpPanierActions
				.add(item)
		}
	}

	handleAchat() {
		const ids = []
		for (let item of this.props.panier.items) {
			ids.push(item.id)
		}
		this
			.props
			.exmpPanierActions
			.vente(this.state.id_adherent, ids)
	}

	render() {

		const adherents = this
			.props
			.adherents
			.items
			.map(adh => {
				return {
					value: adh.id,
					text: adh.nom_resp_legal + ' ' + adh.prenom_resp_legal
				}
			})
		const ouvrages = this
			.props
			.ouvrages
			.items
			.map(ouvr => {
				return {
					value: ouvr.id,
					text: ouvr.nom + ' ' + ouvr.annee
				}
			})

		return (
			<div style={{
				display: 'flex',
				flexDirection: 'column',
				width: '100%',
				justifyContent: 'center'
			}}>
				<InputGroup
					label='Famille adhérente'
					hint="Recherchez une famille adhérente"
					searchText={this.state.adhSearchText}
					handleUpdateInput={this.handleUpdateInputAdh}
					data={adherents}
					href='/adherents'
					button={true}
				/>

				<InputGroup
					label='Ouvrage'
					hint="Recherchez un ouvrage"
					searchText={this.state.ouvrSearchText}
					handleUpdateInput={this.handleUpdateInputOuvr}

					data={ouvrages}
					href='/ouvrages'
					button={true}
				/>

				<Exemplaires
					ouvrage_id={this.state.id_ouvrage}
					handleSelect={this.handleSelect}
					filterOut={this.props.panier}
				/>
				<RaisedButton
					label="Ajouter au panier"
					style={{
						width:280,
						marginTop: 50,
						marginBottom: 50,
						marginLeft: 'auto',
						marginRight: 'auto'
					}}
					primary={true}
					icon={< CartPlusIcon />}
					onTouchTap={this.handlePanier}
				/>
				<ExemplairesPanier />
				<RaisedButton
					label="Valider la liste d'achats"
					style={{
						width:280,
						marginTop: 50,
						marginBottom: 50,
						marginLeft: 'auto',
						marginRight: 'auto'
					}}
					primary={true}
					icon={< CartIcon />}
					onTouchTap={this.handleAchat}
					disabled={!this.state.id_adherent || (this.props.panier.items.length === 0)}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		adherents: state.adherents,
		ouvrages: state.ouvrages,
		exemplaires: state.exemplaires,
		panier: state.exemplaires_panier
	}
}

function mapDispatchToProps(dispatch) {
	return {
		adhActions: bindActionCreators(AdherentActions, dispatch),
		ouvrActions: bindActionCreators(OuvrageActions, dispatch),
		etatsActions: bindActionCreators(EtatActions, dispatch),
		exempActions: bindActionCreators(ExemplaireActions, dispatch),
		exmpPanierActions: bindActionCreators(ExemplairesPanierActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Ventes)
