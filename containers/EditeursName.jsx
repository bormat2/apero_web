import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import SelectField from 'material-ui/SelectField'
import EditeursActions from '../actions/editeurs'
import autobind from 'autobind-decorator'
import {InputGroup} from '../components/GenericComponents'

@autobind
class EditeursName extends Component {

	static get propTypes() {
			return {
			list: PropTypes.array.isRequired,
			actions: PropTypes.object.isRequired,
			id: PropTypes.string,
			value: PropTypes.string,
			handleChange: PropTypes.func
		}
	}

	constructor(props) {
		super(props)

		this.state = {
			searchText: ''
		}
	}

	componentWillMount() {
		if (typeof this.props.value === 'undefined') {
			return
		}
		if (this.props.value === "") {
			return
		}
		if (typeof this.props.value !== 'undefined') {
			const item = this.props.list.filter(item => item.id === this.props.value)[0]
			this.setState({
				searchText: item.nom
			})
		}
	}

	componentWillReceiveProps(nextProps) {
		if (typeof nextProps.value === 'undefined') {
			return
		}
		if (nextProps.value === "") {
			return
		}
		if (typeof nextProps.value !== 'undefined') {

			if (+nextProps.value !== this.state.selected_id) {
				if (this.state.selected_id)
					return
			}

			if ((this.props.list.length !== 0)) {
				const item = this.props.list.filter(item => item.id === nextProps.value)[0]
				this.setState({
					selected_id: nextProps.value,
					searchText: item.nom
				})
			}
		}
	}

	handleUpdateInput(searchText, dataSource) {
		this.setState({searchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({selected_id: id})
		if (!this.props.handleChange) {
			return
		}
		if (typeof (+this.state.selected_id) === 'number') {
			this.props.handleChange({
				target: {
					id: 'id_editeur',
					value: id
				}
			})
		}
	}

	render() {
		if (this.props.id) {
			const item = this.props.list.filter(item => item.id === this.props.id)[0]
			const name = item? item.nom :""
			return (
				<span> {name} </span>
			)
		} else {
			const editeurs = this
				.props
				.list
				.map(ed => {
					return {
						value: ed.id,
						text: ed.nom
					}
				})
			return (
				<InputGroup
					label='Éditeur'
					hint="Sélectionnez l'editeur de l'ouvrage"
					searchText={this.state.searchText}
					handleUpdateInput={this.handleUpdateInput}
					data={editeurs}
					href='/editeurs'
					button={false}
				/>
			)
		}
	}
}

function mapStateToProps(state) {
	return {list: state.editeurs.items}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(EditeursActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(EditeursName)
