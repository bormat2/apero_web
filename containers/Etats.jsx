import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import EtatActions from '../actions/etats'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import autobind from 'autobind-decorator'

@autobind
class Etats extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Etats des ouvrages'

		setImmediate(() => {
			props.actions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}

				<FormDrawer
					form={this.state.openForm?
						<ItemForm
							colsConfig={colsConfig}
							item={this.props.data.items[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.etats}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(EtatActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Etats)

const colsConfig = [
	{
		colName: 'Libellé',
		tooltip: "Le libellé de l'état",
		attrName: 'lib'
	}, {
		colName: 'Décote',
		tooltip: "La décote de l'état",
		attrName: 'decote',
		unit: '%'
	}
]
