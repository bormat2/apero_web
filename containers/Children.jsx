import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import EnfantActions from '../actions/enfants'
import ClassesActions from '../actions/classes'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import autobind from 'autobind-decorator'
import ClassesName from './ClassesName'

@autobind
class Enfants extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired,
			famille_id: PropTypes.number.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Enfants'

		setImmediate(() => {
			props.actions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div style={{width: '100%'}}>
				{table}

				<FormDrawer
					form={this.state.openForm?
						<ItemForm
							colsConfig={colsConfig}
							item={this.filtered[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.enfants}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(EnfantActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Enfants)

const colsConfig = [
	{
		colName: 'Nom',
		tooltip: "Le nom de l'enfant",
		attrName: 'nom'
	}, {
		colName: 'Prénom',
		tooltip: "Le prénom de l'enfant",
		attrName: 'prenom'
	}, {
		colName: 'Classe',
		tooltip: "La classe de l'enfant",
		attrName: 'id_classe',
		container: ClassesName
	}
]
