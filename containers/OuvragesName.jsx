import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import SelectField from 'material-ui/SelectField'
import OuvrageActions from '../actions/ouvrages'

class OuvragesName extends Component {

	static get propTypes() {
			return {
			list: PropTypes.array.isRequired,
			actions: PropTypes.object.isRequired,
			id: PropTypes.string.isRequired
		}
	}

	render() {
		const item = this.props.list.filter(item => item.id === this.props.id)[0]
		const name = item? item.nom+' '+item.annee :""
		return (
			<span> {name} </span>
		)
	}
}

function mapStateToProps(state) {
	return {list: state.ouvrages.items}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(OuvrageActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(OuvragesName)
