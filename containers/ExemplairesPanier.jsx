import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import ExemplairePanierActions from '../actions/exemplaires_panier'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import OuvragesName from './OuvragesName'
import EtatsName from './EtatsName'
import autobind from 'autobind-decorator'
import CartIcon from 'material-ui/svg-icons/action/shopping-cart'

@autobind
class ExemplairesPanier extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Panier'
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.exemplaires_panier}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ExemplairePanierActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ExemplairesPanier)

const colsConfig = [
	{
		colName: 'Ouvrage',
		tooltip: "Le nom de l'ouvrage",
		attrName: 'id_ouvrage',
		container: OuvragesName
	}, {
		colName: 'État',
		tooltip: "L'état de l'ouvrage",
		attrName: 'id_etat',
		container: EtatsName
	}
]
