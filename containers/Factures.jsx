import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import FactureActions from '../actions/factures'
import AdherentActions from '../actions/adherents'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import autobind from 'autobind-decorator'
import AdherentsName from './AdherentsName'

@autobind
class Factures extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired,
			adhActions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Factures des ventes'

		setImmediate(() => {
			props.actions.fetch()
			props
				.adhActions
				.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.factures}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(FactureActions, dispatch),
		adhActions: bindActionCreators(AdherentActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Factures)

const colsConfig = [
	{
		colName: 'N° de facture',
		tooltip: "Le numéro de la facture",
		attrName: 'n_facture'
	}, {
		colName: 'Date',
		tooltip: "Date de la facture",
		attrName: 'date_vente',
		type: 'date'
	}, {
		colName: 'Adhérent',
		tooltip: "L'adhérent ayant effectué l'achat",
		attrName: 'id_adherent',
		container: AdherentsName
	}
]
