import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import SectionsActions from '../actions/sections'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import autobind from 'autobind-decorator'

@autobind
class Sections extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Liste des sections'

		setImmediate(() => {
			props.actions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}

				<FormDrawer
					form={this.state.openForm?
						<ItemForm
							colsConfig={colsConfig}
							item={this.props.data.items[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.sections}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(SectionsActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Sections)

const colsConfig = [
	{
		colName: 'Libellé',
		tooltip: "Libellé de la section",
		attrName: 'lib'
	}
]
