import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import OuvrageActions from '../actions/ouvrages'
import MatieresActions from '../actions/matieres'
import EditeursActions from '../actions/editeurs'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import ItemForm from '../components/ItemForm'
import MatieresName from './MatieresName'
import EditeursName from './EditeursName'
import autobind from 'autobind-decorator'

@autobind
class Ouvrages extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = ItemForm
		this.title = 'Ouvrages'

		setImmediate(() => {
			props.actions.fetch()
			props.matieresActions.fetch()
			props.editeursActions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}

				<FormDrawer
					form={this.state.openForm?
						<ItemForm
							colsConfig={colsConfig}
							item={this.props.data.items[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.ouvrages}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(OuvrageActions, dispatch),
		matieresActions: bindActionCreators(MatieresActions, dispatch),
		editeursActions: bindActionCreators(EditeursActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Ouvrages)

const colsConfig = [
	{
		colName: 'Nom',
		tooltip: "Le nom de l'ouvrage",
		attrName: 'nom'
	}, {
		colName: 'Année',
		tooltip: "L'année de parution de l'ouvrage",
		attrName: 'annee'
	}, {
		colName: 'Matière',
		tooltip: "La matière dont traite l'ouvrage",
		attrName: 'id_matiere',
		container: MatieresName
	}, {
		colName: 'Prix neuf',
		tooltip: "Le prix neuf de l'ouvrage",
		attrName: 'prix_neuf',
		unit: '€'
	}, {
		colName: 'Éditeur',
		tooltip: "L'éditeur de l'ouvrage",
		attrName: 'id_editeur',
		container: EditeursName
	}
]
