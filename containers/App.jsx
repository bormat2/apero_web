import React, {Component} from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Paper from 'material-ui/Paper'
import Header from '../components/Header'
import Adherents from './Adherents'
import Etats from './Etats'
import Etabs from './Etabs'
import Ouvrages from './Ouvrages'
import Classes from './Classes'
import Matieres from './Matieres'
import Sections from './Sections'
import Editeurs from './Editeurs'
import Exemplaires from './Exemplaires'
import Depots from './Depots'
import Ventes from './Ventes'
import Factures from './Factures'

// For Customization Options, edit  or use
// './src/material_ui_raw_theme_file.jsx' as a template.
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import theme from '../src/material_ui_raw_theme_file'

let basePath = '/'
if (process.env.NODE_ENV === "production") {
	basePath = '/apero_web/static/'
}

const flexCSS = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	justiyContent: 'center'
}

const Home = () => (
	<div style={flexCSS}>
		<Paper style={{
			...flexCSS,
			maxWidth: 900,
			marginTop: 12,
			width: '100%'
		}}>
			<h1>Bienvenu dans le gestionnaire de bourse aux livres</h1>
		</Paper>
	</div>
)

const ExemplairesVendus = () => (
	<Exemplaires
		filter='vendus'
	/>
)
const ExemplairesInvendus = () => (
	<Exemplaires
		filter='invendus'
	/>
)

const App = () => (
	<Router>
		<div>
			<MuiThemeProvider muiTheme={theme}>
				<div>
					<Header/>
					<Route exact path={basePath} component={Home}/>
					<Route exact path="*/depots" component={Depots}/>
					<Route exact path="*/adherents" component={Adherents}/>
					<Route exact path="*/ouvrages" component={Ouvrages}/>
					<Route exact path="*/classes" component={Classes}/>
					<Route exact path="*/matieres" component={Matieres}/>
					<Route exact path="*/sections" component={Sections}/>
					<Route exact path="*/editeurs" component={Editeurs}/>
					<Route exact path="*/etats" component={Etats}/>
					<Route exact path="*/etabs" component={Etabs}/>
					<Route exact path='*/ventes' component={Ventes}/>
					<Route exact path='*/vendus' component={ExemplairesVendus}/>
					<Route exact path='*/invendus' component={ExemplairesInvendus}/>
					<Route exact path='*/factures' component={Factures}/>
				</div>
			</MuiThemeProvider>
		</div>
	</Router>
)

export default App
