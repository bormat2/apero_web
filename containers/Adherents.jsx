import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import TextField from 'material-ui/TextField'
import AdherentActions from '../actions/adherents'
import ClassesActions from '../actions/classes'
import TableUI from '../components/TableUI'
import {EditToolBar, FormDrawer} from '../components/GenericComponents'
import Entity from '../components/Entity'
import AdhForm from '../components/AdhForm'
import autobind from 'autobind-decorator'

@autobind
class Adherents extends Entity {

	static get propTypes() {
			return {
			data: PropTypes.object.isRequired,
			actions: PropTypes.object.isRequired,
			classesActions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.colsConfig = colsConfig
		this.formComp = AdhForm
		this.title = 'Familles adherentes'

		setImmediate(() => {
			props.actions.fetch()
			props.classesActions.fetch()
		})
	}

	render() {
		let table = super._render()
		return (
			<div>
				{table}

				<FormDrawer
					width={700}
					form={this.state.openForm?
						<AdhForm
							colsConfig={colsConfig}
							adherent={this.props.data.items[this.state.editingIndex]}
							handleAddit={this._handleAddit}
						/>
					:null}
					handleChange={this._handleToggleForm}
					open={this.state.openForm}
				/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {data: state.adherents}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(AdherentActions, dispatch),
		classesActions: bindActionCreators(ClassesActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Adherents)

const colsConfig = [
	{
		colName: 'Nom',
		tooltip: "Le nom du responsable légal",
		attrName: 'nom_resp_legal'
	}, {
		colName: 'Prénom',
		tooltip: "Le prénom du responsable légal",
		attrName: 'prenom_resp_legal'
	}, {
		colName: 'Dernière adhésion',
		tooltip: "La date de la derniere adhésion",
		attrName: 'date_derniere_adhesion',
		type: 'date'
	}, {
		colName: 'Adresse',
		tooltip: "L'adresse de l'adhérent",
		attrName: 'adresse'
	}, {
		colName: 'Ville',
		tooltip: "La ville de résidence de l'adhérent",
		attrName: 'ville'
	}
]
