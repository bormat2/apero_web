import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import SelectField from 'material-ui/SelectField'
import ClassesActions from '../actions/classes'
import autobind from 'autobind-decorator'
import {InputGroup} from '../components/GenericComponents'

@autobind
class ClassesName extends Component {

	static get propTypes() {
			return {
			list: PropTypes.array.isRequired,
			actions: PropTypes.object.isRequired,
			id: PropTypes.string,
			value: PropTypes.string,
			handleChange: PropTypes.func
		}
	}

	constructor(props) {
		super(props)

		this.state = {
			searchText: ''
		}
	}

	componentWillMount() {
		if (typeof this.props.value === 'undefined') {
			return
		}
		if (this.props.value === "") {
			return
		}
		if (typeof this.props.value !== 'undefined') {
			const item = this.props.list.filter(item => item.id === this.props.value)[0]
			this.setState({
				searchText: item.lib
			})
		}
	}

	componentWillReceiveProps(nextProps) {
		if (typeof nextProps.value === 'undefined') {
			return
		}
		if (nextProps.value === "") {
			return
		}
		if (typeof nextProps.value !== 'undefined') {

			if (+nextProps.value !== this.state.selected_id) {
				if (this.state.selected_id)
					return
			}

			if ((this.props.list.length !== 0)) {
				const item = this.props.list.filter(item => item.id === nextProps.value)[0]
				this.setState({
					selected_id: nextProps.value,
					searchText: item.lib
				})
			}
		}
	}

	handleUpdateInput(searchText, dataSource) {
		this.setState({searchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({selected_id: id})
		if (!this.props.handleChange) {
			return
		}
		if (typeof (+this.state.selected_id) === 'number') {
			this.props.handleChange({
				target: {
					id: 'id_classe',
					value: id
				}
			})
		}
	}

	render() {
		if (this.props.id) {
			const item = this.props.list.filter(item => item.id === this.props.id)[0]
			const name = item? item.lib :""
			return (
				<span> {name} </span>
			)
		} else {
			const classes = this
				.props
				.list
				.map(cl => {
					return {
						value: cl.id,
						text: cl.lib
					}
				})
			return (
				<InputGroup
					label='Classe'
					hint="Sélectionnez la classe de l'enfant"
					searchText={this.state.searchText}
					handleUpdateInput={this.handleUpdateInput}
					data={classes}
					href='/classes'
					button={false}
				/>
			)
		}
	}
}

function mapStateToProps(state) {
	return {list: state.classes.items}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ClassesActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassesName)
