import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import AdherentActions from '../actions/adherents'
import OuvrageActions from '../actions/ouvrages'
import EtatActions from '../actions/etats'
import ExemplaireActions from '../actions/exemplaires'
import Exemplaires from './Exemplaires'
import RaisedButton from 'material-ui/RaisedButton'
import DepotIcon from 'material-ui/svg-icons/action/system-update-alt'
import autobind from 'autobind-decorator'
import {InputGroup} from '../components/GenericComponents'

@autobind
class Depots extends Component {

	static get propTypes() {
		return {
			adherents: PropTypes.object.isRequired,
			ouvrages: PropTypes.object.isRequired,
			etats: PropTypes.object.isRequired,
			adhActions: PropTypes.object.isRequired,
			ouvrActions: PropTypes.object.isRequired,
			etatActions: PropTypes.object.isRequired
		}
	}

	constructor(props) {
		super(props)

		this.state = {
			adhSearchText: '',
			ouvrSearchText: '',
			etatSearchText: ''
		}

		setImmediate(() => {
			props
				.adhActions
				.fetch()
			props
				.ouvrActions
				.fetch()
			props
				.etatActions
				.fetch()
		})
	}

	handleUpdateInputAdh(searchText, dataSource) {
		this.setState({adhSearchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({id_adherent: id})
	}

	handleUpdateInputOuvr(searchText, dataSource) {
		this.setState({ouvrSearchText: searchText})
		const id = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))[0].value
		this.setState({id_ouvrage: id})
	}

	handleUpdateInputEtat(searchText, dataSource) {
		let results = dataSource.filter(obj => (obj.text.toLowerCase().indexOf(searchText.toLowerCase()) !== -1))
		let id = "0"
		if (results.length > 1) {
			for (let r of results) {
				if (r.text === searchText) {
					id = r.value
				}
			}
		} else {
			id = results[0].value
		}
		this.setState({id_etat: id, etatSearchText: searchText}) // TODO : fix wrong id detection...
	}

	handleDepot() {
		this
			.props
			.exempActions
			.addit({
				id_adherent: this.state.id_adherent,
				id_ouvrage: this.state.id_ouvrage,
				id_etat: this.state.id_etat
			})
	}

	render() {

		const adherents = this
			.props
			.adherents
			.items
			.map(adh => {
				return {
					value: adh.id,
					text: adh.nom_resp_legal + ' ' + adh.prenom_resp_legal
				}
			})
		const ouvrages = this
			.props
			.ouvrages
			.items
			.map(ouvr => {
				return {
					value: ouvr.id,
					text: ouvr.nom + ' ' + ouvr.annee
				}
			})

		const etats = this
			.props
			.etats
			.items
			.map(etat => {
				return {value: etat.id, text: etat.lib}
			})

		const ok = !(this.state.id_adherent && this.state.id_ouvrage && this.state.id_etat)

		return (
			<div style={{
				display: 'flex',
				flexDirection: 'column',
				width: '100%',
				justifyContent: 'center'
			}}>
				<InputGroup
					label='Famille adhérente'
					hint="Recherchez une famille adhérente"
					searchText={this.state.adhSearchText}
					handleUpdateInput={this.handleUpdateInputAdh}
					data={adherents}
					href='/adherents'
					button={true}
				/>

				<InputGroup
					label='Ouvrage'
					hint="Recherchez un ouvrage"
					searchText={this.state.ouvrSearchText}
					handleUpdateInput={this.handleUpdateInputOuvr}

					data={ouvrages}
					href='/ouvrages'
					button={true}
				/>

				<InputGroup
					label='État'
					hint="Choisissez un état"
					searchText={this.state.etatSearchText}
					handleUpdateInput={this.handleUpdateInputEtat}
					data={etats}
					href='/etats'
					button={true}
				/>

				<RaisedButton
					label="Déposer un exemplaire"
					style={{
						width:280,
						marginTop: 50,
						marginBottom: 50,
						marginLeft: 'auto',
						marginRight: 'auto'
					}}
					primary={true}
					icon={< DepotIcon />}
					onTouchTap={this.handleDepot}
					disabled={ok}
				/>
				<Exemplaires adherent_id={this.state.id_adherent}/>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {adherents: state.adherents, ouvrages: state.ouvrages, etats: state.etats}
}

function mapDispatchToProps(dispatch) {
	return {
		adhActions: bindActionCreators(AdherentActions, dispatch),
		ouvrActions: bindActionCreators(OuvrageActions, dispatch),
		etatActions: bindActionCreators(EtatActions, dispatch),
		exempActions: bindActionCreators(ExemplaireActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Depots)
