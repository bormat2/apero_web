import getDataRequest from '../middleware/apiFetch'

export default function(types, api_strings) {
	class EntityActions {
		constructor() {
			this.add = data => {
				return {
					type: types.ADD,
					...data
				}
			}
			this._del = id => {
				return {type: types.DELETE, id}
			}

			this.edit = data => {
				return {
					type: types.EDIT,
					...data
				}
			}

			this.request = () => {
				return {type: types.REQUEST}
			}

			this.receive = data => {
				return {
					type: types.RECEIVE,
					data: data,
					receivedAt: Date.now()
				}
			}

			this.select = selectedRows => {
				return {type: types.SELECT, selectedRows: selectedRows}
			}

			this.success = e => {
				return {type: types.SUCCESS}
			}

			this.err = e => {
				return {type: types.ERROR, err: e}
			}

			this.fetch = () => {
				return dispatch => {
					dispatch(this.request())
					getDataRequest({
						name: api_strings.fetch,
						params: [],
						fetch: true
					}, data => {
						dispatch(this.success())
						dispatch(this.receive(data))
					})
				}
			}

			this.addit = _data => {
				const data = {
					..._data
				}
				delete data.children
				return dispatch => {
					dispatch(this.edit(data))
					dispatch(this.request())

					getDataRequest({
						name: api_strings.addit,
						data: data
					}, result => {
						if (!result.error) {
							dispatch(this.success())
							//dispatch(this.receive(result.data))
							if (!_data.id) {
								dispatch(this.add({
									...data,
									id: result.id
								}))
							}
						} else {
							dispatch(this.err(result.error))
						}
					})
				}
			}

			this.del = id => {
				return dispatch => {
					dispatch(this._del(id))
					getDataRequest({
						name: api_strings.del,
						data: {
							id: id
						}
					}, () => {})
				}
			}
		}
	}
	return new EntityActions()
}
