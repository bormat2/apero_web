import * as _types from '../constants/ActionTypes'
import {FACTURES as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'vente_json',
	addit: 'vente_addit',
	del: 'del_vente'
})

export default actions
