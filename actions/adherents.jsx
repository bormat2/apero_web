import * as _types from '../constants/ActionTypes'
import {ADHERENTS as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'famille_adherente_json',
	addit: 'famille_adherente_addit',
	del: 'del_famille_adherente'
})

export default actions
