import * as _types from '../constants/ActionTypes'
import {MATIERES as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'matiere_json',
	addit: 'matiere_addit',
	del: 'del_matiere'
})

export default actions
