import * as _types from '../constants/ActionTypes'
import {EDITEURS as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'editeur_json',
	addit: 'editeur_addit',
	del: 'del_editeur'
})

export default actions
