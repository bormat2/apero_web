import * as _types from '../constants/ActionTypes'
import {SECTIONS as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'section_json',
	addit: 'section_addit',
	del: 'del_section'
})

export default actions
