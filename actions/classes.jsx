import * as _types from '../constants/ActionTypes'
import {CLASSES as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'classes_json',
	addit: 'classe_addit',
	del: 'del_classe'
})

export default actions
