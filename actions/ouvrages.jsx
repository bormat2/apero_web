import * as _types from '../constants/ActionTypes'
import {OUVRAGES as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'ouvrage_json',
	addit: 'ouvrage_addit',
	del: 'del_ouvrage'
})

export default actions
