import * as _types from '../constants/ActionTypes'
import {EXEMPLAIRES_PANIER as suffix} from '../constants/EntityTypes'
import entity from './entity'
import getDataRequest from '../middleware/apiFetch'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'none_json',
	addit: 'none_addit',
	del: 'del_none'
})

actions.vente = (id_adherent, ids) => {

	console.log(id_adherent, ids)
	return dispatch => {

		getDataRequest({
			name: 'vente',
			data: {
				id_adherent: id_adherent,
				exemplaires: ids.join('_')
			}
		}, () => {
			getDataRequest({
				name: 'exemplaire_json',
				params: [],
				fetch: true
			}, data => {
				dispatch({
					type: "RECEIVE_EXEMPLAIRES",
					data: data,
					receivedAt: Date.now()
				})

				ids.map(id => dispatch(actions._del(id)))
			})
		})
	}
}

export default actions
