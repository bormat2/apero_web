import * as _types from '../constants/ActionTypes'
import {ETABS as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'etab_json',
	addit: 'etablissement_addit',
	del: 'del_etablissement'
})

export default actions
