import * as _types from '../constants/ActionTypes'
import {EXEMPLAIRES as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'exemplaire_json',
	addit: 'exemplaire_addit',
	del: 'del_exemplaire'
})

export default actions
