import * as _types from '../constants/ActionTypes'
import {ENFANTS as suffix} from '../constants/EntityTypes'
import entity from './entity'

const types = _types.applySuffix(suffix, {
	..._types
})

const actions = entity(types, {
	fetch: 'enfant_json',
	addit: 'enfant_addit',
	del: 'del_enfant'
})

export default actions
