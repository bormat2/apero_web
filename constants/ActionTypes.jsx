export const ADD = 'ADD'
export const DELETE = 'DELETE'
export const EDIT = 'EDIT'
export const SELECT = 'SELECT'

// Fetch STATUS
export const SUCCESS = 'SUCCESS'
export const ERROR = 'ERROR'
export const RECEIVE = 'RECEIVE'
export const REQUEST = 'REQUEST'

export const applySuffix = (suffix, _types) => {
	const types = _types
	delete types.applySuffix

	let newTypes = {}
	for (let type in types) {
		newTypes[type] = type+suffix
	}
	return newTypes
}
