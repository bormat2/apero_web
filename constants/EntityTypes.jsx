export const ADHERENTS = '_ADHERENTS'
export const ETATS = '_ETATS'
export const ETABS = '_ETABS'
export const CLASSES = '_CLASSES'
export const OUVRAGES = '_OUVRAGES'
export const MATIERES = '_MATIERES'
export const SECTIONS = '_SECTIONS'
export const EDITEURS = '_EDITEURS'
export const EXEMPLAIRES = '_EXEMPLAIRES'
export const ENFANTS = '_ENFANTS'
export const EXEMPLAIRES_PANIER = '_EXEMPLAIRES_PANIER'
export const FACTURES = '_FACTURES'
