<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin");

function protect($string){
	return $string;
	return htmlentities($string);
}

function protectKey($string){
	return preg_replace('/[^a-z0-9A-Z_ ]/i', '', $string);
}

class MainModel {
	private $pdo;

	function __construct(){
		include('__databaseLogin.php');
	}

	function executeReq(){
		$json = json_decode($_GET['json'],true);
		$name = $json['name'];
		$datas = $json['data'] ?? [];
		$paramsStrArr = [];
		$params = [];
		foreach ($datas as $key => $value) {
			$params[] = protect($value);
			$paramsStrArr[] = '@'.protectKey($key).' := ?';
		}
		$paramsStr = count($paramsStrArr) ? 'Set '.implode(',',$paramsStrArr).';' : '';
		$sql = $paramsStr."call $name()";
		$prep = $this->pdo->prepare($sql);
		try{
			$ret = [];
			if(!$prep->execute($params)){
				$ret['error'] = $prep->error;
			}
			if($json['fetch'] ??0){
			   	$ret['response'] = json_decode($prep->fetchColumn());
			}
			$tableName = str_replace("_addit", "", "$name");
			unset($prep);
			if($tableName != $name){
				$prep2 = $this->pdo->prepare('select MAX(id) from '.htmlentities($tableName));
				$prep2->execute();
				$ret['id'] = $prep2->fetchColumn();;
			}
		}catch(Exception $e){
		    $ret['error'] = ($e->getMessage());
		}
	   	echo(json_encode($ret));
	}
};

$db = new MainModel();
$db->executeReq();
 ?>
