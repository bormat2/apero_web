<div>
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin");

function protect($string){
	return $string;
	return htmlentities($string);
}

function protectKey($string){
	return preg_replace('/[^a-z0-9A-Z_ ]/i', '', $string);
}

class MainModel {
	private $pdo;

	function __construct(){
		include('__databaseLogin.php');
	}

	function executeReq(){
		$params = [$_GET['n_facture']];

		$sql = "select *,ouvrage.nom
		from exemplaire ex
		left join etat on ex.id_etat = etat.id
		left join ouvrage on ouvrage.id = ex.id_ouvrage

		where n_facture=?";
		$prep = $this->pdo->prepare($sql);
		try{
			$ret = [];
			if(!$prep->execute($params)){
				$ret['error'] = $prep->error;
			}
			$res = $prep->fetchAll();
			echo "<h2>N° facture ".$_GET['n_facture']."</h2>";
			//echo "<h4>Nom de l'adhérent : ".$res[0]['fnom'].' '.$res[0]['fnom2']."</h4>";
			echo "<table border='1'>";
			echo "<tr><td>".'NOM'."</td><td>".'PRIX NEUF'."</td><td>".'PRIX'."</td></tr>";
			$total = 0;
			foreach ($res as $re) {
				$prix = $re['prix_neuf'] * ($re['decote']/100);
				$total += $prix;
				echo "<tr><td>".$re['nom']."</td><td>".$re['prix_neuf']."</td><td>".$prix."</td></tr>";
			}
			echo "<tr><td colspan=\"2\">TOTAL</td><td> $total</td></tr>";
			echo "</table>";

		}catch(Exception $e){
		    $ret['error'] = ($e->getMessage());
		}
	}
};

$db = new MainModel();
$db->executeReq();
 ?>
 </div>
 <style>
 	h2 {
		text-align: center;
	}
 	body {
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: flex-start;
		padding: 50px;
	}
 	div {
		font-family: sans-serif;
	}
	table {
		padding: 0;
		border-collapse: collapse;
	}
	td {
    	border: 2px solid black;
		height: 30px;
		padding: 5px;
	}
	td:nth-child(2), td:nth-child(3){
    	width: 90px;
		text-align: right;
	}
	tr:first-child > td {
		font-weight: 900;
		text-align: center;
	}
	tr:last-child > td   {

			font-weight: 900;
	}
 </style>
