import {combineReducers} from 'redux'
import createEntitiesWithNamedType from './entities'
import * as types from '../constants/EntityTypes'

const rootReducer = combineReducers({
	adherents: createEntitiesWithNamedType(types.ADHERENTS),
	etats: createEntitiesWithNamedType(types.ETATS),
	etabs: createEntitiesWithNamedType(types.ETABS),
	classes: createEntitiesWithNamedType(types.CLASSES),
	ouvrages: createEntitiesWithNamedType(types.OUVRAGES),
	matieres: createEntitiesWithNamedType(types.MATIERES),
	sections: createEntitiesWithNamedType(types.SECTIONS),
	editeurs: createEntitiesWithNamedType(types.EDITEURS),
	exemplaires: createEntitiesWithNamedType(types.EXEMPLAIRES),
	exemplaires_panier: createEntitiesWithNamedType(types.EXEMPLAIRES_PANIER),
	enfants: createEntitiesWithNamedType(types.ENFANTS),
	factures: createEntitiesWithNamedType(types.FACTURES)
})

export default rootReducer
