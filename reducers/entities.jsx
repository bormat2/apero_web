import * as types from '../constants/ActionTypes'
import * as suffixes from '../constants/EntityTypes'

const initialState = {
	isFetching: false,
	items: [],
	selectedRows: []
}

export default function createEntitiesWithNamedType(entitiesName = '') {
	return function entities(state = initialState, action) {
		switch (action.type) {
			case types.REQUEST + entitiesName:
				return {
					...state,
					isFetching: true
				}

			case types.RECEIVE + entitiesName:
				return {
					...state,
					isFetching: false,
					items: action.data,
					lastUpdated: action.receivedAt
				}

			case types.SELECT + entitiesName:
				return {
					...state,
					selectedRows: action.selectedRows
				}

			case types.ERROR + entitiesName:
				return {
					...state,
					err: action.err
				}

			case types.SUCCESS + entitiesName:
				return {
					...state,
					err: undefined
				}

			case types.DELETE + entitiesName:
			case types.EDIT + entitiesName:
			case types.ADD + entitiesName:
				return {
					...state,
					items: items(state.items, action, entitiesName)
				}

			default:
				return state
		}
	}
}

const items = (state = [], action, itemsName) => {
	switch (action.type) {

		case types.ADD + suffixes.ADHERENTS:
			return [
				...state,
				adherent(undefined, action)
			]
		case types.ADD + suffixes.ETATS:
			return [
				...state,
				etat(undefined, action)
			]
		case types.ADD + suffixes.ETABS:
			return [
				...state,
				etab(undefined, action)
			]
		case types.ADD + suffixes.CLASSES:
			return [
				...state,
				classe(undefined, action)
			]
		case types.ADD + suffixes.OUVRAGES:
			return [
				...state,
				ouvrage(undefined, action)
			]
		case types.ADD + suffixes.MATIERES:
			return [
				...state,
				matiere(undefined, action)
			]
		case types.ADD + suffixes.SECTIONS:
			return [
				...state,
				section(undefined, action)
			]
		case types.ADD + suffixes.EDITEURS:
			return [
				...state,
				editeur(undefined, action)
			]
		case types.ADD + suffixes.EXEMPLAIRES:
			return [
				...state,
				exemplaire(undefined, action)
			]
		case types.ADD + suffixes.EXEMPLAIRES_PANIER:
			return [
				...state,
				exemplaire_panier(undefined, action)
			]
		case types.ADD + suffixes.ENFANTS:
			return [
				...state,
				enfant(undefined, action)
			]
		case types.DELETE + itemsName:
			return state.filter(item => item.id !== action.id)

		case types.EDIT + itemsName:
			const newItem = {
				...action
			}
			delete newItem.type
			return state.map(item => item.id === action.id
				? {
					...item,
					...newItem
				}
				: item)

		default:
			return state
	}
}


const extractProperties = (obj,props) => {
	let res = {}
	for(let prop of props){
		res[prop]= obj[prop]
	}
	return res;
}
const adherent = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.ADHERENTS:
			let ret = extractProperties(action,['id','nom_resp_legal','prenom_resp_legal',
				'date_derniere_adhesion','adresse','ville','children'])
			ret.children = []
			return ret
		default:
			return state
	}
}
const etab = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.ETABS:
			let ret = extractProperties(action,['nom','tel','id'])
			return ret
		default:
			return state
	}
}
const etat = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.ETATS:
			let ret = extractProperties(action,["id","lib","decote"])
			return ret
		default:
			return state
	}
}
const classe = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.CLASSES:
			let ret = extractProperties(action,['id','lib','id_etabliss','id_section','ville'])
			return ret
		default:
			return state
	}
}

const ouvrage = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.OUVRAGES:
			let ret = extractProperties(action,['id','nom','annee','id_matiere','prix_neuf','id_editeur'])
			return ret
		default:
			return state
	}
}

const matiere = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.MATIERES:
			let ret = extractProperties(action,['id','nom'])
			return ret
		default:
			return state
	}
}
const section = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.SECTIONS:
			let ret = extractProperties(action,['id','lib'])
			return ret
		default:
			return state
	}
}
const editeur = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.EDITEURS:
			let ret = extractProperties(action,['id','nom'])
			return ret
		default:
			return state
	}
}
const exemplaire = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.EXEMPLAIRES:
			let ret = extractProperties(action,["id","n_facture","id_etat","id_adherent","id_ouvrage"])
			return ret
		default:
			return state
	}
}
const exemplaire_panier = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.EXEMPLAIRES_PANIER:
			let ret = extractProperties(action,["id","n_facture","id_etat","id_adherent","id_ouvrage"])
			return ret
		default:
			return state
	}
}
const enfant = (state = {}, action) => {
	switch (action.type) {
		case types.ADD + suffixes.ENFANTS:
			let ret = extractProperties(action,["id","nom","prenom","id_famille","id_classe"])
			return ret
		default:
			return state
	}
}
